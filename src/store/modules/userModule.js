const state = {
  // 登录用户
  user: '',
};
const mutations = {
  SET_USER (state, user) {
    state.user = user;
  }
};
const acitons = {};
const getters = {
  userinfo: (state) => state.user.user
};

export default {
  state,
  mutations,
  acitons,
  getters
}