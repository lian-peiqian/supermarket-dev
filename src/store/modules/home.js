// import { reqCategoryList } from '@/api'
import ACTIONS from '@/store/actions' 
// home模块的仓库
const state = {
  // 菜单数据
  categoryList: [],
};
// mutations是唯一修改state的地方
const mutations = {
  GETCATEGORYLIST (state, categoryList) {
    state.categoryList = categoryList;    
  },
  CLEAR_GATEGORYLIST (state) {
    state.categoryList = []
  }
};
// actions|用户处理派发actions的地方，可以书写异步语句、自己逻辑的地方
const actions = {
  async getCategoryList ({ commit }, username) {
    let result = await ACTIONS.GET_DATA({commit},{
      dataUrl: 'levelmenuinfo',
      username: username
    })
    // let result = await reqCategoryList(username)
    if (result.code == '0000') {
      commit("GETCATEGORYLIST", result.data) 
    }
  }
};
// 计算属性
const getters = {

};

export default {
    state,
    mutations,
    actions,
    getters,
};