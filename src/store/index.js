import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'

Vue.use(Vuex)

import home from './modules/home'
import userModule from './modules/userModule'

export default new Vuex.Store({
  actions,
  modules: {
    home,
    userModule,
  }
})
