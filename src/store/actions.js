import { ajaxPost,ajaxGet } from "@/api/axios";

const HttpUrl = 'http://127.0.0.1:8000/'

const ERR_OK = '0000'

const ERR_NOROLE = '0005'

const actions = {
  // eslint-disable-next-line no-unused-vars
  GET_DATA ({ commit }, { dataUrl, ...params }) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve) => {
      const url = `${HttpUrl}${dataUrl}`
      const res = await ajaxPost(url, {
        ...params,
      })
      if (res.data) {
        if (res.data.code === ERR_OK) {
          // if (res.data.data === null) {
          //   res.data.data = []
          // }
          resolve(res.data)
        } else if (res.data.code === ERR_NOROLE) {
        //   noRoleAction(res.data.message)
          resolve(false)
        }
      } else {
        resolve(false)
      }
    })
  },
    
    // eslint-disable-next-line no-unused-vars
  _GET_DATA ({ commit }, { dataUrl, ...params }) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve) => {
      const url = `${HttpUrl}${dataUrl}`
      const res = await ajaxGet(url, {
        ...params,
      })
      if (res.data) {
        if (res.data.code === ERR_OK) {
          if (res.data.data === null) {
            res.data.data = []
          }
          resolve(res.data)
        } else if (res.data.code === ERR_NOROLE) {
        //   noRoleAction(res.data.message)
          resolve(false)
        }
      } else {
        resolve(false)
      }
    })
  },
}

export default actions