import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 全局function
import './api/global'

// plugins
import './plugins/iview'
import './plugins/BaseComponents.js'

Vue.config.productionTip = false

var vue = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

export default vue
