import Vue from 'vue'
import { Message, Notice } from 'iview'

import md5 from 'md5'

const globalFunction = {}
globalFunction.install = (Vue) => {

  /**
   * @description: 加密
   * @param {encryptMode} 加密模式（0：md5,1：sm2,2：sm3,3：sm4）
   * @param {str} 加密内容
   */
  Vue.prototype.$handleEncrypt = (str, encryptMode = 0) => {
    let encryptData = ''

    switch (encryptMode) {
      case 0:
        encryptData = md5(str)
        break
      // case 1:
      //   encryptData = sm2.doEncrypt(str, publicKey, cipherMode)
      //   break
      // case 2:
      //   encryptData = sm3(str)
      //   break
      // case 3:
      //   encryptData = sm4.encryptData_ECB('admin1234')
      //   break
      default:
        encryptData = md5(str)
    }
    return encryptData
  }

  /**
   * @description: 解密
   * @param {encryptMode} 解密模式（0：md5,1：sm2,2：sm3,3：sm4）
   * @param {str} 解密内容
   */
  Vue.prototype.$handleDecrypt = (str, decryptMode = 0) => {
    let decryptData = ''
    switch (decryptMode) {
      case 0:
        decryptData = str
        break
      // case 1:
      //   decryptData = sm2.doDecrypt(str, privateKey, cipherMode)
      //   break
      // case 3:
      //   decryptData = sm4.decryptData_ECB(str)
      //   break
      default:
        decryptData = str
    }
    return decryptData
  }

  // 成功提示消息
  Vue.prototype.$successMessage = (str = '保存成功', duration = 3) => {
    return Message.success({
      content: str,
      duration: duration, 
    })
  }
  // 警告提示消息
  Vue.prototype.$warningMessage = (str = '', duration = 3) => {
    return Message.warning({
      content: str,
      duration: duration, 
    })
  }
  // 错误提示消息
  Vue.prototype.$errorMessage = (str = '', duration = 3) => {
    return Message.error({
      content: str,
      duration: duration,
    })
  }
  // 成功提示框
  Vue.prototype.$successNotice = (str = '', duration = 3) => {
    return Notice.success({
      title: '提示',
      desc: str,
      duration: duration,
    })
  }
  // 警告提示框
  Vue.prototype.$warningNotice = (str = '', duration = 3) => {
    return Notice.warning({
      title: '提示',
      desc: str,
      duration: duration,
    })
  }
  // 错误提示框
  Vue.prototype.$errorNotice = (str = '', duration = 3) => {
    return Notice.error({
      title: '提示',
      desc: str,
      duration: duration,
    })
  }
}

Vue.use(globalFunction)