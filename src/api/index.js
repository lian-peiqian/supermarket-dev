import requests from "./request";

// 三级联动接口
///api/product/getBaseCategoryList get 无参数

// export const reqCategoryList = () => {
//    return requests({ url: 'menulevelinfo', method: 'get' });
// }

export const reqCategoryList = (username) => {
   return requests({ url: 'levelmenuinfo', method: 'post', data: { username: username } });
}


// 当前这个函数需要接受外部传递参数
// 当前这个接口 （获取搜索模块的数据），在每一个组件身上至少有一个默认参数 [可以是空对象]
// params形参：是当用户派发action的时候，第二个参数传递过来的，至少是一个空对象
export const reqGetSearchInfo = (params) => requests({url:'list',method:"post",data:params})