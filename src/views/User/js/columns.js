export const columns = (that) => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'username',
      title: '用户名',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'name',
      title: '姓名',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'level',
      title: '用户等级',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'action',
      title: '操作',
      align: 'center',
      fixed: 'right',
      width: '275',
      render: (h, params) => {
        return h('div', [
          h(
            'Button',
            {
              props: {
                type: 'primary',
                size: 'small',
              },
              style: {
                marginRight: '5px',
              },
              on: {
                click: () => {
                  that.editUser(params.row, params.index)
                },
              },
            },
            '编辑'
          ),
          h(
            'CustomPopTipButton',
            {
              props: {
                confirmTitle: params.row.status === 1 ? '确定禁用？' : '确定启用?',
                btnType: params.row.status === 1 ? 'error' : 'success',
                btnSize: 'small',
                btnText: params.row.status === 1 ? '禁用' : '启用',
                // icon: 'md-trash',
                // disabled: params.row.name === '系统管理员'
              },
              on: {
                ok: () => {
                  that.deleteUser(params.row.username)
                },
              },
            },
            '删除'
          ),
        ])
      }
    }
  ]
}