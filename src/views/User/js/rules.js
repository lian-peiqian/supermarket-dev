const pwdCheckValidate = that => {
  return (rule, value, callback) => {
    if (!value) {
      callback(new Error('确认密码不能为空'));
    } else if (that.formData.password != value) {
      callback(new Error('新密码和确认密码应相同'));
    } else {
      callback();
    }
  }
}

export const rules = that => {
  return {
    username: [
      {
        required: true,
        message: '用户名不能为空',
        trigger: 'blur'
      }
    ],
    name: [
      {
        required: true,
        message: '姓名不能为空',
        trigger: 'blur'
      }
    ],
    level: [
      {
        required: true,
        message: '用户等级不能为空',
        trigger: 'change',
        type: 'number'
      }
    ],
    password: [
      {
        required: true,
        message: '密码不能为空',
        trigger: 'blur'
      }
    ],
    againPwd: [
      {
        required: true,
        validator: pwdCheckValidate(that),
        trigger: 'blur'
      }
    ]
  }
}