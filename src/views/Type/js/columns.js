export const columns = (that) => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'id',
      title: 'id',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'name',
      title: '名称',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'action',
      title: '操作',
      align: 'center',
      fixed: 'right',
      width: '275',
      render: (h, params) => {
        return h('div', [
          h(
            'Button',
            {
              props: {
                type: 'primary',
                size: 'small',
                // disabled: params.row.name == '管理员'
              },
              style: {
                marginRight: '5px'
              },
              on: {
                click: () => {
                  that.editType(params.row, params.index)
                }
              }
            },
            '编辑'
          ),
          h(
            'CustomPopTipButton',
            {
              props: {
                confirmTitle: '确定删除？',
                btnType: 'error',
                btnSize: 'small',
                btnText: '删除',
                // icon: 'md-trash',
                // disabled: params.row.name === '管理员'
              },
              on: {
                ok: () => {
                  that.deleteType(params.row.id)
                },
              },
            },
            '删除'
          )
        ])
      }
    }
  ]
}