export const columns = (that) => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'picture',
      title: '图片',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
            h('Avatar', {
              props: {
                src: 'http://127.0.0.1:8000' + params.row.picture,
                size: "large",
                shape: "square"
              },
            })
          ])
        }
    },
    {
      key: 'name',
      title: '名称',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'date',
      title: '交易时间',
      minWidth: 140,
      align: 'center',
    },
    {
      key: 'type',
      title: '类别',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'preferential',
      title: '是否优惠',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.preferential ? '是' : '否'
        ])
      }
    },
    {
      key: 'price',
      title: '总价格',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.price.toFixed(2) + '元'
        ])
      }
    },
    {
      key: 'sum',
      title: '数量',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.sum + params.row.unitId
        ])
      }
    },
    {
      key: 'username',
      title: '操作人员',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'action',
      title: '操作',
      align: 'center',
      fixed: 'right',
      width: '120',
      render: (h, params) => {
        return h('div', [
          h(
            'CustomPopTipButton',
            {
              props: {
                confirmTitle: '确定删除？',
                btnType: 'error',
                btnSize: 'small',
                btnText: '删除',
                // icon: 'md-trash',
                // disabled: params.row.name === '管理员'
              },
              on: {
                ok: () => {
                  that.deleteOrder(params.row.id)
                },
              },
            },
            '删除'
          )
        ])
      }
    }
  ]
}