const pfPriceValidate = that => {
  return (rule, value, callback) => {
    // 必须是商品有优惠
    if (that.formData.preferential == true) {
      if (!value) {
        callback(new Error('优惠后价格不能为空'));
      } else if (that.formData.price <= value) {
        callback(new Error('优惠后价格不能大于或等于原价格'))
      } else {
        callback()
      }
    }
  }
}

export const addRules = that => {
  return {
    name: [
      {
        required: true,
        message: '商品名不能为空',
        trigger: 'blur'
      }
    ],
    unit: [
      {
        required: true,
        message: '商品单位不能为空',
        trigger: 'change',
        type: 'number'
      }
    ],
    type: [
      {
        required: true,
        message: '商品类别不能为空',
        trigger: 'change',
        type: 'number'
      }
    ],
    preferential: [
      {
        required: true,
        message: '商品名不能为空',
        trigger: 'blur',
        type: 'boolean'
      }
    ],
    preferentialPrice: [
      {
        validator: pfPriceValidate(that),
        trigger: 'blur'
      }
    ]
  }
}

export const editRules = that => {
  return {
    name: [
      {
        required: true,
        message: '商品名不能为空',
        trigger: 'blur'
      }
    ],
    unit: [
      {
        required: true,
        message: '商品单位不能为空',
        trigger: 'change',
        type: 'number'
      }
    ],
    type: [
      {
        required: true,
        message: '商品类别不能为空',
        trigger: 'change',
        type: 'number'
      }
    ],
    preferential: [
      {
        required: true,
        message: '商品名不能为空',
        trigger: 'blur',
        type: 'boolean'
      }
    ],
    preferentialPrice: [
      {
        validator: pfPriceValidate(that),
        trigger: 'blur'
      }
    ],
    
  }
}