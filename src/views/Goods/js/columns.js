export const columns = (that) => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'picture',
      title: '图片',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
            h('Avatar', {
              props: {
                src: 'http://127.0.0.1:8000' + params.row.picture,
                size: "large",
                shape: "square"
              },
            })
          ])
        }
    },
    {
      key: 'name',
      title: '名称',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'type',
      title: '类别',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'price',
      title: '价格',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.price.toFixed(2) + '元'
        ])
      }
    },
    {
      key: 'preferential',
      title: '是否优惠',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.preferential ? '是' : '否'
        ])
      }
    },
    {
      key: 'preferentialPrice',
      title: '优惠后价格',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.preferentialPrice.toFixed(2) + '元'
        ])
      }
    },
    {
      key: 'sum',
      title: '库存',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.sum + params.row.unitId
        ])
      }
    },
    {
      key: 'soldSum',
      title: '已售出',
      minWidth: 120,
      align: 'center',
      render: (h, params) => {
        return h('div', [
          params.row.soldSum + params.row.unitId
        ])
      }
    },
    {
      key: 'action',
      title: '操作',
      align: 'center',
      fixed: 'right',
      width: '275',
      render: (h, params) => {
        return h('div', [
          h(
            'Button',
            {
              props: {
                type: params.row.isShelves ? 'warning' : 'success',
                size: 'small',
                // disabled: params.row.name == '管理员'
              },
              style: {
                marginRight: '5px'
              },
              on: {
                click: () => {
                  that._changeGoodsShelves(params.row.id)
                }
              }
            },
            params.row.isShelves ? '下架' : '上架'
          ),
          h(
            'Button',
            {
              props: {
                type: 'primary',
                size: 'small',
                // disabled: params.row.name == '管理员'
              },
              style: {
                marginRight: '5px'
              },
              on: {
                click: () => {
                  that.editGoods(params.row, params.index)
                }
              }
            },
            '编辑'
          ),
          h(
            'CustomPopTipButton',
            {
              props: {
                confirmTitle: '确定删除？',
                btnType: 'error',
                btnSize: 'small',
                btnText: '删除',
                // icon: 'md-trash',
                // disabled: params.row.name === '管理员'
              },
              on: {
                ok: () => {
                  that.deleteGoods(params.row.id)
                },
              },
            },
            '删除'
          )
        ])
      }
    }
  ]
}