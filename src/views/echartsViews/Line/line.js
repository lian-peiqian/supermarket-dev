import * as echarts from 'echarts'
const lineBase = function (xData, seriesData) {
    xData = xData || [
        '2/10','2/11','2/12','2/13','2/14','2/15','2/16','2/17','2/18','2/19'
    ]
    seriesData = seriesData || [
        35,26,25,16,50,35,26,25,16,50
    ]
    const option = {
        title: {
            show: false
        },
        legend: {
            //设置布局方式，默认水平布局， 可选值： ‘horizontal’(水平) | ‘vertical’ (垂直)
            orient: 'horizontal',
            left: 'left',
            padding: [0,0,0,250],
            itemWidth: 19.2,    //设置图标的宽
            itemHeight: 6.9,   //设置图标的高
            // itemRadius: 3.5,  //图标圆角设置无效
            // icon: 'arrow',  //设置图标样式
            textStyle: {
                fontSize: 10.24,
                color: '#000'  // 图例文字颜色
            },
            itemGap: 10,  //设置各个item之间的间隔，单位px，默认为10，横向布局时为水平间隔，纵向布局时为纵向间隔。
            data: ['销售额'],
        },
        tooltip: {
            show: true,
        },
      xAxis: {
        type: 'category',
        boundaryGap: false,
            data: xData,
            axisLabel: {
                inside: false,
                fontSize: 10,
                textStyle: {
                    color: '#000'
                },
                interval: 0,
                rotate: 30
            },
            axisTick: {
                show: false
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#000'
                }
            },
            z: 10,
            
        },
        yAxis: {
            name: '（元）',
            nameTextStyle: {
                color: "#000",
                fontSize: 12.7,
                fontFamily: 'Alibaba-Regular'
            },
            axisLine: {
                show: true,
            },
            splitLine: {
                show: true
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                textStyle: {
                    color: '#000'
                }
            }
        },
        dataZoom: [
            {
                type: 'inside'
            }
        ],
        grid:{          //设置图标距离边框的距离
            x:40,
            y:30,
            x2:0,
            y2:50
        },
      series: [
            
            {
                name: '销售额',
                type: 'line',
                smooth: true,
                showBackground: true,
                itemStyle: {
                    color: 'rgb(1, 191, 236)'
                },
                areaStyle: {
                  opacity: 0.8,
                  color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                    {
                      offset: 0,
                      color: 'rgba(128, 255, 165, 0.5)'
                    },
                    {
                      offset: 1,
                      color: 'rgba(1, 191, 236, 0.5)'
                    }
                  ])
                },
                backgroundStyle: {
                        color: '#191E49',
                },
                barWidth: 8,
                emphasis: {
                    itemStyle: {
                        // color: new echarts.graphic.LinearGradient(
                        //     0, 0, 0, 1,
                        //     [
                        //         {offset: 0, color: '#A3FFB9'},
                        //         {offset: 1, color: '#020008'}
                        //     ]
                        // )
                    }
                },
                data: seriesData
            }
        ]
    };
    return option
}

export const textLine = (xData,seriesData)  =>({
    ...lineBase(xData,seriesData,0)
})