const codeVaild = that => {
  return (rule, value, callback) => {
    if (!value) {
      callback(new Error('请输入验证码'))
    } else if (
      value.toUpperCase() !== that.$refs.validateCode.code.toUpperCase()
    ) {
      callback(new Error('验证码输入错误'))
    } else {
      callback()
    }
  }
}


export const rules = that => {
  return {
    id: [
      {
        required: true,
        message: '账号不能为空',
        trigger: 'blur'
      }
    ],
    password: [
      {
        required: true,
        message: '密码不能为空',
        trigger: 'blur'
      }
    ],
    code: [
      {
        required: true,
        validator: codeVaild(that),
        trigger: 'blur'
      }
    ]
  }
}