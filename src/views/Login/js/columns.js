export const columns = () => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'username',
      title: '用户名',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'level',
      title: '用户等级',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'time',
      title: '登录时间',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'ip',
      title: 'ip地址',
      minWidth: 120,
      align: 'center',
    }
  ]
}