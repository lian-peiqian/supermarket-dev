export const columns = (that) => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'id',
      title: 'id',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'name',
      title: '名称',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'pid',
      title: '父级页面',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'path',
      title: '页面路径',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'action',
      title: '操作',
      align: 'center',
      fixed: 'right',
      width: '275',
      render: (h, params) => {
        return h('div', [
          h(
            'Button',
            {
              props: {
                type: 'primary',
                size: 'small',
              },
              style: {
                marginRight: '5px',
              },
              on: {
                click: () => {
                  that.editPage(params.row, params.index)
                },
              },
            },
            '编辑'
          ),
          h(
            'CustomPopTipButton',
            {
              props: {
                confirmTitle: '确定删除？',
                btnType: 'error',
                btnSize: 'small',
                btnText: '删除',
                // icon: 'md-trash',
                // disabled: params.row.name === '系统管理员'
              },
              on: {
                ok: () => {
                  that.deletePage(params.row.id)
                },
              },
            },
            '删除'
          ),
        ])
      }
    }
  ]
}


// 编辑角色菜单
export const columns1 = (that) => {
  return [
    {
      type: 'index',
      title: '序号',
      width: 80,
      align: 'center',
    },
    {
      key: 'id',
      title: 'id',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'name',
      title: '名称',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'pid',
      title: '父级页面',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'path',
      title: '页面路径',
      minWidth: 120,
      align: 'center',
    },
    {
      key: 'action',
      title: '操作',
      align: 'center',
      fixed: 'right',
      width: '120',
      render: (h, params) => {
        return h('div', [
          h(
            'Button',
            {
              props: {
                type: that.selectedMenu.indexOf(params.row.id) < 0 ? 'primary' : 'error',
                size: 'small',
              },
              style: {
                marginRight: '5px',
              },
              on: {
                click: () => {
                  if (that.selectedMenu.indexOf(params.row.id) < 0) {
                    that.selected(params.row, params.index)
                  } else {
                    that.unSelect(params.row, params.index)
                  }
                },
              },
            },
            that.selectedMenu.indexOf(params.row.id) < 0 ? '选中' : '移除'
          )
        ])
      }
    }
  ]
}