import Login from '@/views/Login/login.vue'
import Home from '@/views/Home/home.vue'
import Search from '@/views/Search/search.vue'
import User from '@/views/User/user.vue'
import AddUser from '@/views/User/adduser.vue'
import PageManage from '@/views/Page/pageManage.vue'
import AddPage from '@/views/Page/handlePage/addPage.vue'
import LoginInfo from '@/views/Login/handleLogin/logininfo.vue'
import LevelMenu from '@/views/Page/handlePage/levelMenu.vue'
import Level from '@/views/Level/level.vue'
import Unit from '@/views/Unit/unit.vue'
import Type from '@/views/Type/type.vue'
import Goods from '@/views/Goods/goods.vue'
import Order from '@/views/Order/order.vue'
import AddOrder from '@/views/Order/handleOrder/addOrder'

export default [
    {
        path: '/login',
        component: Login,
        name: 'login',
        meta:{show:false,isLogin:false,name:'登录'}
    },
    {
        path: "/home",
        component: Home,
        name: 'home',
        meta:{show:true,isLogin:true,name:'首页'}
  },
  {
    path: "/user",
    component: User,
    name: 'user',
    meta:{show:true,isLogin:true,name:'用户管理'}
  },
  {
    path: "/adduser",
    component: AddUser,
    name: 'adduser',
    meta:{show:true,isLogin:true,name:'新增用户'}
  },
  {
    path: "/pagemanage",
    component: PageManage,
    name: 'pagemanage',
    meta:{show:true,isLogin:true,name:'页面管理'}
  },
  {
    path: "/addpage",
    component: AddPage,
    name: 'addpage',
    meta:{show:true,isLogin:true,name:'新增页面'}
  },
  {
    path: "/logininfo",
    component: LoginInfo,
    name: 'logininfo',
    meta:{show:true,isLogin:true,name:'登录日志'}
  },
  {
    path: '/levelmenu',
    component: LevelMenu,
    name: 'levelmenu',
    meta: {show:true,isLogin: true,name: '角色菜单管理'}
  },
  {
    path: '/level',
    component: Level,
    name: 'level',
    meta: {show:true,isLogin: true,name: '角色权限管理'}
  },
  {
    path: '/unit',
    component: Unit,
    name: 'unit',
    meta: {show:true,isLogin: true,name: '商品单位管理'}
  },
  {
    path: '/goodstype',
    component: Type,
    name: 'type',
    meta: {show:true,isLogin: true,name: '商品类别管理'}
  },
  {
    path: '/goodsmanage',
    component: Goods,
    name: 'goods',
    meta: {show:true,isLogin: true,name: '商品管理'}
  },
  {
    path: '/order',
    component: Order,
    name: 'order',
    meta: {show:true,isLogin: true,name: '订单管理'}
  },
  {
    path: '/addorder',
    component: AddOrder,
    name: 'addorder',
    meta: {show:true,isLogin: true,name: '生成订单'}
  },
    {
        path: "/search/:keyword?",
        component: Search,
        meta: { show: false,isLogin:true },
        name: 'search',
        // 路由组件能不能传递props数据？
        // 布尔值写法：params
        // props：true
        // 对象写法：额外的给路由组件传递一些props
        // props：{ a: 1, b: 2 }，
        // 函数写法： 可以params参数、query参数，通过props传递给路由组件
        props: ($route) => {
            return { keyword: $route.params.keyword, k: $route.query.k };
        }
    },
    // 重定向，在项目跑起来的时候，访问/，立马让他定向到首页
    {
        path: '*',
        redirect: "/home",
    }
]