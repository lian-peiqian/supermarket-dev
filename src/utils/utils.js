

/**
 * @description: localstorage存储数据
 * @param {Object, String}
 * @return:
 */
 export const setKey = (name, obj) => {
  // name = `${moduleName}-${name}`
  let object = JSON.stringify(obj)
  localStorage.setItem(name, object)
}

/**
 * @description: localstorage获取数据
 * @param {Object, String}
 * @return:
 */
export const getKey = (name) => {
  // name = `${moduleName}-${name}`
  let object = localStorage.getItem(name)
  object = object ? JSON.parse(object) : false
  return object
}

/**
 * @description: localstorage删除数据
 * @param {Object, String}
 * @return:
 */
export const removeKey = (name) => {
  // name = `${moduleName}-${name}`
  localStorage.remove(name)
}

/**
 * @description: localstorage清除数据
 * @param {Object, String}
 * @return:
 */
export const clearKey = () => {
  localStorage.clear()
}
