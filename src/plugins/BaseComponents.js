import Vue from 'vue'
import CustomPopTipButton from '@/base/CustomPopTipButton'

const plugins = {}
plugins.install = (Vue) => {
  Vue.component('CustomPopTipButton', Vue.extend(CustomPopTipButton))
}

Vue.use(plugins)